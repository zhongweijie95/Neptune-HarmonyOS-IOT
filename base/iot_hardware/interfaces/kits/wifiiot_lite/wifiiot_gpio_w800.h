#ifndef WIFIIOT_GPIO_W800_H
#define WIFIIOT_GPIO_W800_H

typedef enum {
    WIFI_IOT_GPIO_PA_00 = 0, /**< gpio a0 */
    WIFI_IOT_GPIO_PA_01,     /**< gpio a1 */
    WIFI_IOT_GPIO_PA_02,     /**< gpio a2 */
    WIFI_IOT_GPIO_PA_03,     /**< gpio a3 */
    WIFI_IOT_GPIO_PA_04,     /**< gpio a4 */
    WIFI_IOT_GPIO_PA_05,     /**< gpio a5 */
    WIFI_IOT_GPIO_PA_06,     /**< gpio a6 */
    WIFI_IOT_GPIO_PA_07,     /**< gpio a7 */
    WIFI_IOT_GPIO_PA_08,     /**< gpio a8 */
    WIFI_IOT_GPIO_PA_09,     /**< gpio a9 */
    WIFI_IOT_GPIO_PA_10,     /**< gpio a10 */
    WIFI_IOT_GPIO_PA_11,     /**< gpio a11 */
    WIFI_IOT_GPIO_PA_12,     /**< gpio a12 */
    WIFI_IOT_GPIO_PA_13,     /**< gpio a13 */
    WIFI_IOT_GPIO_PA_14,     /**< gpio a14 */
    WIFI_IOT_GPIO_PA_15,     /**< gpio a15 */

    WIFI_IOT_GPIO_PB_00, /**< gpio b0 */
    WIFI_IOT_GPIO_PB_01, /**< gpio b1 */
    WIFI_IOT_GPIO_PB_02, /**< gpio b2 */
    WIFI_IOT_GPIO_PB_03, /**< gpio b3 */
    WIFI_IOT_GPIO_PB_04, /**< gpio b4 */
    WIFI_IOT_GPIO_PB_05, /**< gpio b5 */
    WIFI_IOT_GPIO_PB_06, /**< gpio b6 */
    WIFI_IOT_GPIO_PB_07, /**< gpio b7 */
    WIFI_IOT_GPIO_PB_08, /**< gpio b8 */
    WIFI_IOT_GPIO_PB_09, /**< gpio b9 */
    WIFI_IOT_GPIO_PB_10, /**< gpio b10 */
    WIFI_IOT_GPIO_PB_11, /**< gpio b11 */
    WIFI_IOT_GPIO_PB_12, /**< gpio b12 */
    WIFI_IOT_GPIO_PB_13, /**< gpio b13 */
    WIFI_IOT_GPIO_PB_14, /**< gpio b14 */
    WIFI_IOT_GPIO_PB_15, /**< gpio b15 */
    WIFI_IOT_GPIO_PB_16, /**< gpio b16 */
    WIFI_IOT_GPIO_PB_17, /**< gpio b17 */
    WIFI_IOT_GPIO_PB_18, /**< gpio b18 */
    WIFI_IOT_GPIO_PB_19, /**< gpio b19 */
    WIFI_IOT_GPIO_PB_20, /**< gpio b20 */
    WIFI_IOT_GPIO_PB_21, /**< gpio b21 */
    WIFI_IOT_GPIO_PB_22, /**< gpio b22 */
    WIFI_IOT_GPIO_PB_23, /**< gpio b23 */
    WIFI_IOT_GPIO_PB_24, /**< gpio b24 */
    WIFI_IOT_GPIO_PB_25, /**< gpio b25 */
    WIFI_IOT_GPIO_PB_26, /**< gpio b26 */
    WIFI_IOT_GPIO_PB_27, /**< gpio b27 */
    WIFI_IOT_GPIO_PB_28, /**< gpio b28 */
    WIFI_IOT_GPIO_PB_29, /**< gpio b29 */
    WIFI_IOT_GPIO_PB_30, /**< gpio b30 */
    WIFI_IOT_GPIO_PB_31  /**< gpio b31 */
} W800IotGpioName;

typedef enum {
    WIFI_IOT_GPIO_DIR_OUTPUT, /**< output */
    WIFI_IOT_GPIO_DIR_INPUT   /**< input */
} W800IotGpioDir;

typedef enum {
    WIFI_IOT_GPIO_ATTR_FLOATING, /**< floating status */
    WIFI_IOT_GPIO_ATTR_PULLHIGH, /**< pull high */
    WIFI_IOT_GPIO_ATTR_PULLLOW,  /**< pull low */
} W800IotGpioAttr;

#endif /* end of WIFIIOT_GPIO_W800_H*/