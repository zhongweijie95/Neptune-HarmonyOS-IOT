## 购买渠道

官方淘宝店铺：【润和芯片社区HopeRun】HiSpark开发套件购买链接：

| 产品                                                         | 链接                                             |
| ------------------------------------------------------------ | ------------------------------------------------ |
| ① HarmonyOS HiSpark AI Camera开发套件 （Hi3516DV300）        | https://item.taobao.com/item.htm?id=622922688823 |
| ② HarmonyOS HiSpark IPC DIY开发套件 （Hi3518EV300）          | https://item.taobao.com/item.htm?id=623376454933 |
| ③ HarmonyOS HiSpark Wi-Fi IoT智能家居开发套件 （Hi3861V100） | https://item.taobao.com/item.htm?id=622343426064 |
| ④ Neptune HarmonyOS 物联网 IOT模组                           | https://item.taobao.com/item.htm?id=635868903111 |



## 许可与版权

- 本项目所有代码使用[**BSD 3-Clause License**](https://opensource.org/licenses/BSD-3-Clause)许可证发布；

- 本项目所有文档使用[**CC BY-NC-SA**](https://creativecommons.org/licenses/by-nc-sa/4.0/)许可证发布，即《知识共享
许可证》的“**署名-非商业性使用-相同方式共享**”版本；

  

## 反馈与改进

- **问题反馈**
  - 如果您发现了本项目中的错误或不当之处，可以在本项目的issues页面进行反馈：https://gitee.com/hihopeorg/Neptune-HarmonyOS-IOT/issues
- **参与改进**
  - 您也可以通过fork本仓，修改后向本仓发起`Pull Request`请求合并；或者使用码云的轻量级PR，直接在本仓修改，修改完>发起 PR；
